package com.example.gallery;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class FullImage extends Activity{
	int position1;
	String name;
	int[] image_res = { R.drawable.pic_1, R.drawable.pic_2, R.drawable.pic_3,
			R.drawable.pic_4, R.drawable.pic_5, R.drawable.pic_6,
			R.drawable.pic_7, R.drawable.pic_8, R.drawable.pic_9,
			R.drawable.pic_10, R.drawable.pic_11, R.drawable.pic_12,
			R.drawable.pic_13, R.drawable.pic_14, R.drawable.pic_15 };

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.largeimage);
		position1= getIntent().getIntExtra("position", 0);
		name= getIntent().getStringExtra("image_name");
		System.out.println(position1);
		showImage(position1,name);
	}
	
	private void showImage(int position, String Image_name){
		ImageView imageview= (ImageView) findViewById(R.id.largeimage1);
		TextView name= (TextView) findViewById(R.id.image_name);
		imageview.setImageResource(image_res[position]);
		name.setText(Image_name);
		//imageview.setVisibility(position);
		

}}
