package com.example.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	GridView grid;
	String[] image_name = { "kate", "bean", "brad", "Angliena", "5", "jacks", "Aish", "Wolverien",
			"9", "leonardo", "11", "12", "13", "14", "15" };
	int[] image_res = { R.drawable.pic_1, R.drawable.pic_2, R.drawable.pic_3,
			R.drawable.pic_4, R.drawable.pic_5, R.drawable.pic_6,
			R.drawable.pic_7, R.drawable.pic_8, R.drawable.pic_9,
			R.drawable.pic_10, R.drawable.pic_11, R.drawable.pic_12,
			R.drawable.pic_13, R.drawable.pic_14, R.drawable.pic_15 };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		grid = (GridView) findViewById(R.id.grid);
		ImageAdapter imageAdapter = new ImageAdapter(image_name, image_res);
		grid.setAdapter(imageAdapter);
		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				//showImage(view,position);
				System.out.println(position+" " +id);
				Intent intent = new Intent(MainActivity.this,FullImage.class);
				intent.putExtra("position", position);
				intent.putExtra("image_name",image_name[position]);
				startActivity(intent);
				
			}
			
		});
	}
	
	/*private void showImage(View view, int position){
		ImageView imageview= (ImageView) findViewById(R.id.largeimage);
		imageview.setImageResource(image_res[position]);
		imageview.setVisibility(position);
		
	}*/

	static class viewholder {

		TextView image_name;
		ImageView image_res;
	}
	
	


	class ImageAdapter extends BaseAdapter {
		String[] image_name;
		int[] image_res;

		public ImageAdapter(String[] name2, int[] face2) {

			// TODO Auto-generated constructor stub
			image_name = name2;

			image_res = face2;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return image_name.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View row = convertView;
			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.image_view, parent, false);

				viewholder holder = new viewholder();
				holder.image_name = (TextView) row
						.findViewById(R.id.image_name);

				holder.image_res = (ImageView) row.findViewById(R.id.image);
			
				holder.image_name
						.setText(MainActivity.this.image_name[position]);
				holder.image_res
						.setImageResource(MainActivity.this.image_res[position]);
				row.setTag(holder);
		
			
			} else {
				viewholder holder = (viewholder) row.getTag();
				holder.image_name
						.setText(MainActivity.this.image_name[position]);

				holder.image_res
						.setImageResource(MainActivity.this.image_res[position]);

			}
			return row;
			
		}
		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
